# hss-benchmark

Program to benchmark LHSS's

## Usage

### Instruction creation
First you need to create an instructions file.
This file describes which file lengths and packet lengths you want to try, and how many runs each opearation has to do. 

To create an instruction file, run `hss-benchmark create-instructions <name> <file lengths> <packet lengths> <num runs>`
, where `file lengths` and `packet lengths` are comman separated numeric values, name is an identifier of yours to mark your instruction file, and `num runs` is a single number.

E.g. `hss-benchmark create-instructions basic 1 1 10` will create an instructions file to run experiments with file length 1, packet length 1, 10 times for each operation. 

`hss-benchmark create-instructions basic 1,2 1,2 100`  will create an instructions file to run (file length, packet length) configurations (1,1) (1,2) (2,1) (2,2) 100 times for each operation.

### Benchark
To start benchmarking, we run `hss-benchmark benchmark <instruction file name> [option <outfile name>]`
The program then reads the provided instruction file, runs the specified benchmarks, and writes output.
If no outfile is provided, output is written to stdout.

E.g. `hss-benchmark benchmark instructions/basic.txt` will run the basic instructions and output to stdout.
`hss-benchmark benchmark instructions/basic.txt out/basic.csv` will run the basic instructions and output the csv file out/basic.csv.

## ToDo
- Multi Key specific benchmarks