use lhss::schemes::{
    aranha19::Ara2019, boneh09::Bon09, catalano12::Cat12, li18::Li18, li20::Li2020, lin17::Lin17,
    lin21::Lin2021, schabhuser17::Sch17, schabhuser18::Schab2018, schabhuser19::Sch2019,
    zhang18::NCZha2018,
};

use crate::benchmark::{BenchCFG, Benchmark, SVCMeasurements};

pub fn run_benchmark(cfg: &mut BenchCFG, scheme_name: &String) -> SVCMeasurements {
    let measurement = match scheme_name.as_str() {
        "ara19" => Ara2019::benchmark_single_signer(cfg),
        "bon09" => Bon09::benchmark_single_signer(cfg),
        "cat12" => Cat12::benchmark_single_signer(cfg),
        "li_18" => Li18::benchmark_single_signer(cfg),
        "li_20" => Li2020::benchmark_single_signer(cfg),
        "lin17" => Lin17::benchmark_single_signer(cfg),
        "lin21" => Lin2021::benchmark_single_signer(cfg),
        "sch17" => Sch17::benchmark_single_signer(cfg),
        "sch18" => Schab2018::benchmark_single_signer(cfg),
        "sch19" => Sch2019::benchmark_single_signer(cfg),
        "zha18" => NCZha2018::benchmark_single_signer(cfg),
        _ => (vec![0], vec![0], vec![0]),
    };
    measurement
}
