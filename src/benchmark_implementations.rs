use std::collections::HashMap;

use bls12_381::Scalar;
use lhss::{
    misc::num_traits::AugmentFile,
    schemes::{
        aranha19::{self, Ara2019},
        boneh09::Bon09,
        catalano12::Cat12,
        li18::Li18,
        li20::{self, Li2020},
        lin17::Lin17,
        lin21::Lin2021,
        schabhuser17::{MLProg, Sch17},
        schabhuser18::{self, Schab2018},
        schabhuser19::{self, Sch2019},
        zhang18::NCZha2018,
    },
};

use crate::{
    benchmark::{BenchCFG, Benchmark, SVCMeasurements},
    misc::{time_and_retrieve, timeit_n_times},
};

impl Benchmark for Ara2019 {
    fn benchmark_single_signer(cfg: &mut BenchCFG) -> SVCMeasurements {
        let scheme = Self::setup();
        let key = scheme.keygen(&mut cfg.rng);
        let file = &cfg.file;
        let num_runs = cfg.num_runs;
        let coefficients = &cfg.coefficients;
        let labels: Vec<aranha19::Label> = (0..cfg.pl)
            .map(|i| aranha19::Label::new(&cfg.file_id_u8, &(i as u8)))
            .collect();
        let identity_program = vec![Scalar::from(1)];
        let labeled_identity_programs: Vec<aranha19::LabeledProgram> = (0..cfg.pl)
            .map(|i| aranha19::LabeledProgram::new(&identity_program, &vec![labels[i].clone()]))
            .collect();
        let (sigs, sign_durations) = time_and_retrieve(
            || scheme.sign_file_single_signer(&key, &labels, file),
            num_runs,
        );
        let ver_durations = timeit_n_times(
            || scheme.verify_file_single_vk(&labeled_identity_programs, &key.pk, file, &sigs),
            num_runs,
        );
        let com_duraitons = timeit_n_times(
            || scheme.combine_file_signatures(coefficients, &sigs),
            num_runs,
        );
        (sign_durations, ver_durations, com_duraitons)
    }
}
impl Benchmark for Bon09 {
    fn benchmark_single_signer(cfg: &mut BenchCFG) -> SVCMeasurements {
        let bon = Bon09::setup(cfg.pl, &mut cfg.rng);
        let key = bon.keygen(&mut cfg.rng);
        let (sigs, sigs_durations) = time_and_retrieve(
            || bon.sign_file_single_key(&key.sk, &cfg.file_id_bytes, &cfg.file),
            cfg.num_runs,
        );
        let ver_durations = timeit_n_times(
            || bon.sign_file_single_key(&key.sk, &cfg.file_id_bytes, &cfg.file),
            cfg.num_runs,
        );
        let com_durations = timeit_n_times(|| bon.combine(&cfg.coefficients, &sigs), cfg.num_runs);
        (sigs_durations, ver_durations, com_durations)
    }
}

impl Benchmark for Cat12 {
    fn benchmark_single_signer(cfg: &mut BenchCFG) -> SVCMeasurements {
        let file = cfg.file.augment(true);
        let rng = &mut cfg.rng;
        let num_runs = cfg.num_runs;
        let scheme = Self::setup(cfg.pl, cfg.fl);
        let key = scheme.keygen(rng);
        let (sigs, sign_durations) = time_and_retrieve(
            || scheme.sign_file_single_key(&key, &cfg.file_id_scalar, &file, rng),
            num_runs,
        );
        let ver_durations = timeit_n_times(
            || scheme.verify_file_single_vk(&key.vk, &cfg.file_id_scalar, &file, &sigs),
            num_runs,
        );
        let com_durations = timeit_n_times(|| scheme.combine(&cfg.coefficients, &sigs), num_runs);
        (sign_durations, ver_durations, com_durations)
    }
}

impl Benchmark for Li18 {
    fn benchmark_single_signer(cfg: &mut BenchCFG) -> SVCMeasurements {
        let file = cfg.file.augment(true);
        let mut rng = &mut cfg.rng;
        let num_runs = cfg.num_runs;
        let scheme = Self::setup(cfg.pl, cfg.fl, &mut rng);
        let key = scheme.keygen(&mut rng);
        let (sigs, sign_durations) = time_and_retrieve(
            || scheme.sign_file_single_key(&key.sk, &file, &cfg.file_id_scalar, &mut rng),
            num_runs,
        );
        let ver_durations = timeit_n_times(
            || scheme.verify_file_single_vk(&file, &sigs, &cfg.file_id_scalar, &key.vk),
            num_runs,
        );
        let com_durations = timeit_n_times(|| scheme.combine(&cfg.coefficients, &sigs), num_runs);
        (sign_durations, ver_durations, com_durations)
    }
}

impl Benchmark for Li2020 {
    fn benchmark_single_signer(cfg: &mut BenchCFG) -> SVCMeasurements {
        let file = cfg.file.augment(false);
        let num_runs = cfg.num_runs;
        let scheme = Self::setup(&mut cfg.rng);
        let key = scheme.keyext(&cfg.signer_id_bytes, &mut cfg.rng);
        let tag = li20::Tag::new(&cfg.file_id_bytes, &key.big_r);
        let (sigs, sig_durations) = time_and_retrieve(
            || {
                scheme.sign_file_single_key(
                    &key,
                    &cfg.signer_id_bytes,
                    &cfg.file_id_bytes,
                    &file,
                    cfg.pl,
                )
            },
            num_runs,
        );
        let ver_durations = timeit_n_times(
            || scheme.verify_file_single_vk(&cfg.signer_id_bytes, &tag, &file, &sigs, cfg.pl),
            num_runs,
        );
        let com_durations = timeit_n_times(|| scheme.combine(&cfg.coefficients, &sigs), num_runs);
        (sig_durations, ver_durations, com_durations)
    }
}

impl Benchmark for Lin17 {
    fn benchmark_single_signer(cfg: &mut BenchCFG) -> SVCMeasurements {
        let file = cfg.file.augment(false);
        let num_runs = cfg.num_runs;
        let scheme = Self::setup(cfg.pl, cfg.fl, &mut cfg.rng);
        let key_a = scheme.keygen(&mut cfg.rng);
        let key_b = scheme.keygen(&mut cfg.rng);
        let key_c = scheme.keygen(&mut cfg.rng);
        let (sigs, sig_durations) = time_and_retrieve(
            || scheme.sign_file_single_key(&key_a.sk, &key_b.pk, &cfg.file_id_bytes, &file),
            num_runs,
        );
        let ver_durations = timeit_n_times(
            || scheme.verify_file_single_vk(&key_a.pk, &key_b.sk, &cfg.file_id_bytes, &file, &sigs),
            num_runs,
        );
        let com_durations = timeit_n_times(
            || {
                scheme.combine(
                    &key_a.pk,
                    &key_c.pk,
                    &key_b.sk,
                    &cfg.coefficients,
                    &file,
                    &sigs,
                )
            },
            num_runs,
        );
        (sig_durations, ver_durations, com_durations)
    }
}

impl Benchmark for Lin2021 {
    fn benchmark_single_signer(cfg: &mut BenchCFG) -> SVCMeasurements {
        let file = cfg.file.augment(false);
        let num_runs = cfg.num_runs;
        let scheme = Self::setup(cfg.pl, &mut cfg.rng);
        let key_a = scheme.keygen(&mut cfg.rng);
        let key_b = scheme.keygen(&mut cfg.rng);
        let (sigs, sig_durations) = time_and_retrieve(
            || scheme.sign_file_single_key(&key_a.sk, &key_b.pk, &cfg.file_id_bytes, &file),
            num_runs,
        );
        let ver_durations = timeit_n_times(
            || scheme.verify_file_single_vk(&key_a.pk, &key_b.sk, &cfg.file_id_bytes, &file, &sigs),
            num_runs,
        );
        let com_durations = timeit_n_times(
            || scheme.combine(&key_a.pk, &key_b.sk, &file, &sigs, &cfg.coefficients),
            num_runs,
        );
        (sig_durations, ver_durations, com_durations)
    }
}

impl Benchmark for Sch17 {
    fn benchmark_single_signer(cfg: &mut BenchCFG) -> SVCMeasurements {
        let num_runs = cfg.num_runs;
        let scheme = Self::setup(cfg.fl, cfg.pl, &mut cfg.rng);
        let key = scheme.keygen(&mut cfg.rng);
        let (sigs, sig_durations) = time_and_retrieve(
            || scheme.sign_file_single_key(&key.sk, &cfg.file_id_bytes, &cfg.file),
            num_runs,
        );
        let zero_func = vec![Scalar::zero(); cfg.fl];
        let id_progs = (0..cfg.fl)
            .map(|i| {
                let mut res = zero_func.clone();
                res[i] = Scalar::one();
                MLProg::new(res, cfg.file_id_bytes.to_vec())
            })
            .collect();
        let func_prog = MLProg::new(cfg.coefficients.to_vec(), cfg.file_id_bytes.to_vec());

        let ver_durations = timeit_n_times(
            || scheme.verify_file_single_vk(&key.vk, &id_progs, &cfg.file, &sigs),
            num_runs,
        );

        let com_durations = timeit_n_times(|| scheme.eval(&key.vk, &func_prog, &sigs), num_runs);
        (sig_durations, ver_durations, com_durations)
    }
}

impl Benchmark for Schab2018 {
    fn benchmark_single_signer(cfg: &mut BenchCFG) -> SVCMeasurements {
        let max_identities = cfg.fl;
        let num_runs = cfg.num_runs;
        let scheme = Self::setup(max_identities, cfg.fl, cfg.pl, &mut cfg.rng);
        let key = scheme.keygen(&mut cfg.rng);
        let signer_id = cfg.signer_id_bytes[0];
        let labels = (0..cfg.fl)
            .map(|tag| schabhuser18::Label::new(tag, signer_id))
            .collect();
        let mut vks = HashMap::new();
        vks.insert(signer_id, key.pk);
        let (sigs, sig_durations) = time_and_retrieve(
            || {
                scheme.sign_file_single_key(
                    &key.sk,
                    &cfg.file_id_bytes,
                    &labels,
                    &cfg.file,
                    &mut cfg.rng,
                )
            },
            num_runs,
        );
        let mut id_programs = Vec::new();
        for label in labels {
            let prog =
                schabhuser18::MLProg::new(&cfg.file_id_bytes, vec![label], vec![Scalar::one()]);
            id_programs.push(prog);
        }
        let ver_durations = timeit_n_times(
            || scheme.verify_file(&vks, &id_programs, &cfg.file, &sigs),
            num_runs,
        );
        let com_durations = timeit_n_times(|| scheme.combine(&cfg.coefficients, &sigs), num_runs);
        (sig_durations, ver_durations, com_durations)
    }
}

impl Benchmark for Sch2019 {
    fn benchmark_single_signer(cfg: &mut BenchCFG) -> SVCMeasurements {
        let max_identities = cfg.fl;
        let num_runs = cfg.num_runs;
        let scheme = Self::setup(max_identities, cfg.fl, cfg.pl, &mut cfg.rng);
        let key = scheme.keygen(&mut cfg.rng);
        let signer_id = cfg.signer_id_bytes;
        let did = &cfg.file_id_bytes;

        let labels = (0..cfg.fl)
            .map(|tag| schabhuser19::Label::new(signer_id.to_vec(), tag))
            .collect();
        let mut vks = HashMap::new();
        vks.insert(signer_id.to_vec(), key.pk);
        let (sigs, sign_durations) = time_and_retrieve(
            || scheme.sign_file_single_key(&key.sk, did, &labels, &cfg.file, &mut cfg.rng),
            num_runs,
        );
        let id_progs = labels
            .iter()
            .map(|label| {
                schabhuser19::MLProg::new(&did.to_vec(), &vec![label.clone()], &vec![Scalar::one()])
            })
            .collect();
        let ver_durations = timeit_n_times(
            || scheme.verify_file(&vks, &id_progs, &cfg.file, &sigs),
            num_runs,
        );
        let com_durations = timeit_n_times(|| scheme.combine(&cfg.coefficients, &sigs), num_runs);
        (sign_durations, ver_durations, com_durations)
    }
}

impl Benchmark for NCZha2018 {
    fn benchmark_single_signer(cfg: &mut BenchCFG) -> SVCMeasurements {
        let file = cfg.file.augment(false);
        let num_runs = cfg.num_runs;
        let scheme = Self::setup(cfg.pl, cfg.fl, &mut cfg.rng);
        let key = scheme.extract(&cfg.signer_id_bytes.to_vec(), &mut cfg.rng);
        let (sigs, sign_durations) = time_and_retrieve(
            || scheme.sign_file_single_key(&key, &cfg.file_id_bytes, &file),
            num_runs,
        );
        let ver_durations = timeit_n_times(
            || {
                scheme.verify_file_single_vk(
                    &cfg.signer_id_bytes.to_vec(),
                    &cfg.file_id_bytes,
                    &key.hr,
                    &key.gs,
                    &file,
                    &sigs,
                )
            },
            num_runs,
        );
        let com_durations = timeit_n_times(|| scheme.combine(&cfg.coefficients, &sigs), num_runs);
        (sign_durations, ver_durations, com_durations)
    }
}
