use std::time::Instant;

pub fn timeit<F: FnMut() -> T, T>(mut f: F) -> (T, u128) {
    let start = Instant::now();
    let res = f();
    let duration = start.elapsed().as_nanos();
    (res, duration)
}

pub fn timeit_n_times<F: FnMut() -> T, T>(mut f: F, num_runs: usize) -> Vec<u128> {
    let mut durations = Vec::new();
    for _ in 0..num_runs {
        let (_, duration) = timeit(|| f());
        durations.push(duration);
    }
    durations
}

pub fn time_and_retrieve<F: FnMut() -> T, T>(mut f: F, num_runs: usize) -> (T, Vec<u128>) {
    let mut durations = Vec::new();

    let (res, duration) = timeit(|| f());
    durations.push(duration);

    for _ in 1..num_runs {
        let (_, duration) = timeit(|| f());
        durations.push(duration);
    }

    (res, durations)
}
