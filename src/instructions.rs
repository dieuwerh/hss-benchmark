use std::{
    collections::hash_map::DefaultHasher,
    fs::File,
    hash::{Hash, Hasher},
    io::{Read, Write},
    path::Path,
};

use bls12_381::Scalar;
use lhss::misc::util::{random_scalar_file, random_scalar_packet};
use rand::SeedableRng;

use crate::{
    benchmark::BenchCFG,
    benchmark_runner::run_benchmark,
    reporter::{make_writer, Report},
};

pub fn run_instructions(instruction_file_path: &Path, outfile_path: Option<&Path>) {
    let header: Vec<&str> = "SCHEME,OPERATION,DURATION(NS),FL,PL,NUM_RUNS,SINGLE/MK"
        .split(",")
        .collect();
    let mut writer = make_writer(outfile_path);
    writer
        .write_record(&header)
        .expect("Error while writing header row");

    let mut named_configs = parse_instruction_file(instruction_file_path);
    for (cfg, scheme_name) in &mut named_configs {
        let measurement = run_benchmark(cfg, &scheme_name);
        writer.report(measurement, cfg, scheme_name);
    }
}
pub fn parse_instruction_file(file_path: &Path) -> Vec<(BenchCFG, String)> {
    let mut data = String::new();
    let mut f = File::open(file_path).unwrap();
    f.read_to_string(&mut data)
        .expect("Unable to read instruction file");
    let instructions: Vec<String> = data.split(", ").map(|ins| String::from(ins)).collect();
    let seed = file_path.to_str().unwrap().to_string();
    let named_configs: Vec<(BenchCFG, String)> = instructions
        .iter()
        .map(|ins| parse_benchmark_instruction(ins, &seed))
        .collect();
    named_configs
}
pub fn parse_benchmark_instruction(
    instruction: &String,
    seed_string: &String,
) -> (BenchCFG<'static>, String) {
    let split: Vec<&str> = instruction.split("-").collect();
    let scheme_name = split[0].to_string();
    let fl: usize = split[1].parse::<usize>().unwrap();
    let pl: usize = split[2].parse::<usize>().unwrap();
    let num_runs: usize = split[3].parse::<usize>().unwrap();
    let mut rng = rand_chacha::ChaChaRng::seed_from_u64(seed_from_string(seed_string));
    let file_id_u8 = 42u8;
    let file_id_bytes = b"hello this is the file id";
    let file_id_scalar = Scalar::from(42);
    let signer_id_bytes = b"hello this is the signer id";
    let file = random_scalar_file(fl, pl, &mut rng);
    let coefs = random_scalar_packet(fl, &mut rng);
    let cfg = BenchCFG {
        fl,
        pl,
        num_runs,
        file,
        file_id_bytes,
        file_id_u8,
        file_id_scalar,
        signer_id_bytes,
        coefficients: coefs,
        rng: Box::new(rng),
    };
    (cfg, scheme_name.clone())
}

pub fn seed_from_string(string: &String) -> u64 {
    let mut hasher = DefaultHasher::new();
    string.hash(&mut hasher);
    hasher.finish()
}

pub fn create_instructions(
    file_lengths: Vec<usize>,
    packet_lengths: Vec<usize>,
    runs: usize,
    config_name: String,
) {
    let schemes = vec![
        "ara19", "bon09", "cat12", "li_18", "li_20", "lin17", "lin21", "sch17", "sch18", "sch19",
        "zha18",
    ];
    let mut instructions = Vec::new();
    for fl in &file_lengths {
        for pl in &packet_lengths {
            for scheme in &schemes {
                let instruction = format!("{}-{}-{}-{}", scheme, fl, pl, runs);
                instructions.push(instruction);
            }
        }
    }
    let instructions_string = instructions.join(", ");
    let file_path = format!("instructions/{}.txt", config_name);
    let mut f = File::create(&file_path).expect("Unable to create file to store instructions");
    f.write_all(instructions_string.as_bytes())
        .expect("Unable to write data to the instructions file");
    println!("Created instructions at {}", &file_path);
}
