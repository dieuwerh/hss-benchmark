use std::{
    fs::{create_dir_all, File},
    io::{self, Write},
    path::Path,
};

use csv::Writer;

use crate::benchmark::{BenchCFG, SVCMeasurements};

pub fn make_writer(file_path: Option<&Path>) -> Writer<Box<dyn Write>> {
    let write: Box<dyn io::Write> = if let Some(path_ref) = file_path {
        let prefix = path_ref.parent().unwrap();
        create_dir_all(prefix).expect("Error while creating path to outfile");
        Box::new(File::create(path_ref).expect("Error while creating file to report to"))
    } else {
        Box::new(io::stdout())
    };
    let wtr = csv::Writer::from_writer(write);
    wtr
}

pub trait Report {
    fn report(&mut self, measurement: SVCMeasurements, cfg: &mut BenchCFG, scheme_name: &String);
}
impl Report for Writer<Box<dyn Write>> {
    fn report(&mut self, measurement: SVCMeasurements, cfg: &mut BenchCFG, scheme_name: &String) {
        let fl = cfg.fl;
        let pl = cfg.pl;
        let num_runs = cfg.num_runs;
        let (sigs, vers, coms) = measurement;
        for ((s, v), c) in sigs.iter().zip(vers).zip(coms) {
            self.write_record(measurement_to_record(
                scheme_name,
                "SIGN",
                s,
                fl,
                pl,
                num_runs,
                "SINGLE",
            ))
            .expect("Error while writing sign record");
            self.write_record(measurement_to_record(
                scheme_name,
                "VER",
                &v,
                fl,
                pl,
                num_runs,
                "SINGLE",
            ))
            .expect("Error while writing ver record");
            self.write_record(measurement_to_record(
                scheme_name,
                "COM",
                &c,
                fl,
                pl,
                num_runs,
                "SINGLE",
            ))
            .expect("Error while writing com record");
        }
        self.flush().expect("Error while flushing written records");
    }
}

fn measurement_to_record(
    scheme_name: &str,
    operation: &str,
    duration: &u128,
    fl: usize,
    pl: usize,
    num_runs: usize,
    single_mk: &str,
) -> Vec<String> {
    return vec![
        scheme_name.to_string(),
        operation.to_string(),
        duration.to_string(),
        fl.to_string(),
        pl.to_string(),
        num_runs.to_string(),
        single_mk.to_string(),
    ];
}
