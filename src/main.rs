use std::path::Path;

use instructions::run_instructions;

use crate::instructions::create_instructions;
use clap::{Parser, Subcommand};

mod benchmark;
mod benchmark_implementations;
mod benchmark_runner;
mod instructions;
mod misc;
mod reporter;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Runs benchmark instruction file. Runs the relevant benchmarks in the way as instructed, and writes output to a CSV file.
    Benchmark {
        instructions_file: String,
        out_csv: Option<String>,
    },
    /// Creates instructions file based on the provided name, file lenghts (comma separated array), packet lenghts (comma separated array) and the number of runs
    CreateInstructions {
        name: String,
        fls: String,
        pls: String,
        runs: usize,
    },
}
fn main() {
    let cli = Cli::parse();
    match &cli.command {
        Commands::Benchmark {
            instructions_file,
            out_csv,
        } => {
            let outfile_path = if let Some(path) = out_csv {
                Some(Path::new(path.as_str()))
            } else {
                None
            };
            run_instructions(Path::new(instructions_file.as_str()), outfile_path);
        }
        Commands::CreateInstructions {
            name,
            fls,
            pls,
            runs,
        } => {
            let fls: Vec<usize> = fls
                .split(",")
                .map(|fl| fl.parse::<usize>().unwrap())
                .collect();
            let pls: Vec<usize> = pls
                .split(",")
                .map(|pl| pl.parse::<usize>().unwrap())
                .collect();
            create_instructions(fls, pls, *runs, name.clone());
        }
    }
}
