use bls12_381::Scalar;
use lhss::misc::types::File;
use rand::RngCore;

pub type Measurement = Vec<u128>;
pub type SVCMeasurements = (Measurement, Measurement, Measurement);
pub struct BenchCFG<'a> {
    pub fl: usize, // File length
    pub pl: usize, // Packet length
    pub num_runs: usize,
    pub file: File<Scalar>,
    pub file_id_bytes: &'a [u8],
    pub file_id_u8: u8,
    pub file_id_scalar: Scalar,
    pub signer_id_bytes: &'a [u8],
    pub coefficients: Vec<Scalar>,
    pub rng: Box<dyn RngCore>,
}
pub trait Benchmark {
    fn benchmark_single_signer(cfg: &mut BenchCFG) -> SVCMeasurements;
}
